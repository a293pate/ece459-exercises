// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    let mut a = 0;
    let mut b = 1;
    let mut c;
    if n == 0 {
        return a;
    }
    for _i in 2..(n+1) {
       c = a + b;
       a = b;
       b = c;
    }
    b
}


fn main() {
    println!("{}", fibonacci_number(10));
}
