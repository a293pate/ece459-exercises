// You should implement the following function:

fn sum_of_multiples(mut number: i32, multiple1: i32, multiple2: i32) -> i32 
{
    let mut sum: i32 = 0;
    for n in 1..number {
        if n % multiple1 == 0 || n % multiple2 == 0 {
            sum += n;
        }
    }
    sum
}

fn main() {
    println!("{}", sum_of_multiples(1000, 5, 3));
}
