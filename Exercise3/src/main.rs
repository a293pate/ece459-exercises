use std::thread;

static N: i32 = 10;  // number of threads to spawn

// You should modify main() to spawn multiple threads and join() them
fn main() {
    let mut children = vec![];

    for _i in 0..N {
        children.push(thread::spawn(move || {
            println!("idk");
        }));
    }

    for c in children {
        let _ = c.join();
    }
}
